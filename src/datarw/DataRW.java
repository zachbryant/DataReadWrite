package bleat.androidv.utils;


import android.accounts.Account;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Arrays;
import java.util.HashSet;

import bleat.androidv.R;
import bleat.androidv.activity.FrontPageActivity;

/*
An implementation of Java NIO that handles large amounts of data very quickly.
Works for android projects, and has a big of code in the static initializer that
I added for handling multiple users.

*/

public final class FileUtils {
	private final static HashSet<String> filenames;
    public static String DELIMITER = ";";

	private FileUtils(){}
	static{
        filenames = new HashSet<>(
                Arrays.asList(Utils.getStringArray(R.array.fileutilsNames))
        ); //the string array is defined elsewhere in the project
		for(String name:filenames)
			for(Account account : AccountUtils.getAccounts())
				try{store(account.name+"_"+name,"",false);}
				catch(Exception e){e.printStackTrace();}
	}

    //this will store the data given in the file specified. if no such file exists,
    //it was not defined in the project and will not write to a non project file.
    //if specified to overwrite, the contents of the file will be cleared and rewritten
	@SuppressWarnings("unchecked")
	public static void store(String oldFileName, String data, boolean overwrite) throws Exception {
		checkFileName(oldFileName);
		String fileName = fileNameForCurrentUser(oldFileName);
		File dataFile = new File(Utils.getCurrentActivity().getFilesDir(),fileName);
		if(overwrite)
			dataFile.delete();
		if(!dataFile.exists() && !dataFile.createNewFile())
			throw new IOException("Could not create file "+dataFile.getPath());

		data = smoothData(data);
		remove(oldFileName, data.split(DELIMITER));
		RandomAccessFile dataRAF = new RandomAccessFile(dataFile,"rw");
		dataRAF.seek(dataFile.length());
		/*
            RandomAccessFile.write() inherently appends to file. It is recommended that param overwrite
            stays false, unless the data needs to be modified or deleted.
            Will throw an exception if the file name is not standard.
            Will not write line breaks to file; therefore the data will not be very human readable.
        */
		dataRAF.write(data.getBytes());
		dataRAF.close();
	}

    //fetches the entire contents of the file given, if it exists
	public static String load(String oldFileName)throws IOException{
		checkFileName(oldFileName);
		String fileName = fileNameForCurrentUser(oldFileName);

		File dataFile = new File(Utils.getCurrentActivity().getFilesDir(),fileName);
		if(!dataFile.exists())
			if(!dataFile.createNewFile())
				throw new IOException("Could not load or create file "+dataFile.getPath());

		RandomAccessFile dataRAF = new RandomAccessFile(dataFile,"r");
		FileChannel dataChannel = dataRAF.getChannel();
		MappedByteBuffer dataBuff = dataChannel.map(FileChannel.MapMode.READ_ONLY, 0, dataChannel.size());
		byte[] dataBytes = new byte[dataBuff.remaining()];
		dataBuff.get(dataBytes);
		//MappedByteBuffer is meant for long lived or large amounts of data. It will not release
		//the file until it is cleaned up, and
		//does not natively support closure. Lame.
		dataBuff=null;
		System.gc();
		dataChannel.close();
		dataRAF.close();

		String result = new String(dataBytes);
		result = result.replaceAll(DELIMITER+"+$","").replaceAll("^"+DELIMITER+"+","");
		return result;
	}

    //removes the given piece of data from the specified file, ignores data that doesnt exist
    //returns whether or not any change was made
	public static boolean remove(String oldFileName, String id)throws Exception{
		return remove(oldFileName, id.toLowerCase().split(DELIMITER));
	}
	
	//removes each piece of data from the file specified, ignores any data that doesnt exist
	//returns false if no change was made at all
	public static boolean remove(String oldFileName,String[] id)throws Exception{
		checkFileName(oldFileName);
		String fileName = fileNameForCurrentUser(oldFileName);

		String data = load(oldFileName);
		int initLength = data.length();
		String regex ="(";
		for(String _id:id)
			if(!_id.isEmpty())
				regex+=(";*"+_id.toLowerCase()+";*|");

		if(regex.length() > 1)
			regex = regex.substring(0,regex.length()-1);
		regex += ")";

		data = data.replaceAll(regex, ";")
				.replaceAll(DELIMITER+"{2,}", DELIMITER)
				.replaceAll(DELIMITER+"$","");
		if(initLength == data.length())
			return false;
		//store because the user specifically decided to make this action
		store(oldFileName, data, true);
		return true;
	}

    //checks whether the single piece of data exists in the file, returns boolean respectively
	public static boolean contains(String oldFileName, String id)throws Exception{
		HashSet<String> data;
		checkFileName(oldFileName);

		data=new HashSet<>(Arrays.asList(load(oldFileName).split(DELIMITER)));
		for (String next : data) {
			if (next.equals(id))
				return true;
		}
		return false;
	}
	
	//checks whether the file contains the entire list of data given to it in any order
	//and returns the data that does exist
	public static HashSet<String> contains(String oldFileName, String[] id)throws Exception{
		HashSet<String> data;
		checkFileName(oldFileName);

		data=new HashSet<>(Arrays.asList(load(oldFileName).split(DELIMITER)));
		data.retainAll(Arrays.asList(id));
		return data;
	}

    //updates delimiter used when parsing data
	public static void useDelimiter(char d){
		useDelimiter(Character.toString(d));
	}
    
    //updates delimiter used when parsing data
	public static void useDelimiter(String d){
		if(d == null || d.length() == 0)
			DELIMITER = ";";
		else
			DELIMITER = d.substring(0,1);
	}

    //goes through the process of removing all contents from a file
	public static boolean clear(String oldFileName) throws FileNotFoundException{
		checkFileName(oldFileName);

		try{
			store(oldFileName,"",true);
			return true;
		}catch(Exception e){e.printStackTrace();}
		return false;
	}

    //method checks whether a file is empty or not
	public static boolean isClear(String oldFileName){
		try{
			return load(oldFileName).isEmpty();
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}

    //destroys all the files created by this code
	public static void clearAll(){
		for(String fileName : filenames)
			try{
				fileName = fileNameForCurrentUser(fileName);
				clear(fileName);
			}catch(Exception e){e.printStackTrace();}
	}

	private static String smoothData(String raw){
		if(raw.isEmpty())
			return raw;
		raw = raw.trim().replaceAll("\\s+", DELIMITER).toLowerCase();
		return DELIMITER+raw;
	}

	private static void checkFileName(String fileName) throws FileNotFoundException{
		if(!filenames.contains(fileName))
			throw new FileNotFoundException("Incorrect data file name "+fileName);
	}

	private static String fileNameForCurrentUser(String fileName){
		if(!fileName.contains("_"))
			return FrontPageActivity.getActiveAccount()+"_"+fileName;
		return fileName;
	}
}
